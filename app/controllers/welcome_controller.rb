class WelcomeController < ApplicationController
  def index

  end

  def push
    push_notifications
    head :ok
  end

  # For Private channel auth
  def pusher_auth
    params[:client] = 'pusher'
    response = client.auth(params)
    render json: response
  end

  private

  def push_notifications
    return unless client
    sleep 2
    client.publish_message("Message from backend within 2 seconds using #{clients[params[:client]]}")
  end

  def client
    return unless clients.include?(params[:client])
    @client ||= clients[params[:client]].build
  end

  def clients
    {
      'ably' => Push::AblyRest,
      'pusher' => Push::Pusher
    }
  end
end
