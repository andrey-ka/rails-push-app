class Push::AbstractClient

  def publish_message(message)
    raise 'Not Implemented'
  end

  def channel_name
    'test'
  end

  def event_name
    'general'
  end

end