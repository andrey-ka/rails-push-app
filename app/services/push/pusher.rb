class Push::Pusher < Push::AbstractClient

  def self.build
    new
  end

  def publish_message(message)
    client.trigger(channel_name, event_name, message: message)
  end

  def auth(params)
    client.authenticate(params[:channel_name], params[:socket_id])
  end

  private

  def client
    @client ||= Pusher::Client.new(
      app_id: ENV['PUSHER_APP_ID'],
      key: ENV['PUSHER_KEY'],
      secret: ENV['PUSHER_SECRET'],
      cluster: 'eu',
      encrypted: true
    )
  end

  def channel_name
    'private-test'
  end

end