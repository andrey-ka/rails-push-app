class Push::AblyRest < Push::AbstractClient

  def self.build
    new
  end

  def publish_message(message)
    channel.publish(event_name, message)
  end

  private

  def client
    @_client ||= Ably::Rest.new(key: ENV['ABLY_PRIVATE_KEY'])
  end

  def channel
    @channel ||= client.channel(channel_name)
  end


end