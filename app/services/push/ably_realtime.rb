class Push::AblyRealtime

  def self.build
    new
  end

  def publish_message(message)
    EventMachine.run do

      chanel.publish(event_name, message)
    end

  end

  private

  def client
    @_client ||= Ably::Realtime.new(ENV['ABLY_PRIVATE_KEY'])
  end

  def chanel
    @channel ||= client.channels.get('test')
  end

  def event_name
    'general'
  end

end