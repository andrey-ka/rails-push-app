var ablyHandler = function(configId) {
  var ably = new Ably.Realtime(configId);
  var channel = ably.channels.get('test');

  channel.subscribe(function(msg) {
    alert("Received: " + JSON.stringify(msg.data));
  });

  $('#ably-publish').on('click', function (e) {
    channel.publish('test', $('#ably-message').val())
  })
}

$(function () {
    ablyHandler($('#ably').data('config-id'))
  }
);

