var pusherHandler = function(configId) {
  Pusher.logToConsole = true;
  var pusher = new Pusher(configId, {
      cluster: 'eu',
      forceTLS: true,
      auth: {
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
      }
    }
    });
  var channel = pusher.subscribe('private-test');

  channel.bind('general', function(data) {
    alert("Received: " + JSON.stringify(data));
  });

  $('#pusher-publish').on('click', function (e) {
    // Prefixed client events
    channel.trigger('client-general', { data: $('#pusher-message').val() })
  })
}

$(function () {
    pusherHandler($('#pusher').data('config-id'))
  }
);

