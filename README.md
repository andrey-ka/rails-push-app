### Summary

This project is implemented as a playground with Pusher and Ably providers.

### Installation

1. `bundle install`

2. `foreman start`

3. Open [localost:5000](http://localhost:5000) and start sending & receiving messages between Backend & Front End


## Notes
1. All configs for free plans are placed in `.env` file.
2. Pusher Message posting from Client side is triggered as per dev console but not picked up by client subscriber 
(need to investigate further if needed)