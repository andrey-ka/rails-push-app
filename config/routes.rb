Rails.application.routes.draw do
  get 'welcome/index'
  get '/push' => 'welcome#push'
  post '/pusher/auth' => 'welcome#pusher_auth'

  root 'welcome#index'
end
